# Q3Rally

Q3Rally server Linux builds.

## Description  

This project is motivated by the idea to make easy to deploy Q3Rally servers on (GNU/)Linux and provide a base for building in different architectures.

It is focused in providing a minimal build over stable versions of q3rally-server.

## Releases

Each released build is dependent on the main game [Q3Rally](https://github.com/Q3Rally-Team/q3rally).

Additionally, for the container image, if some vulnerability is found for some component external to these dependencies, a new build will be providing indicating it in the tag and replacing the `latest` version.
