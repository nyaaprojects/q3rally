FROM debian:12-slim AS base

ARG DEBIAN_FRONTEND=noninteractive

ARG ARCH_ARG
ARG LANG_ARG
ARG TIMEZONE_ARG
ARG Q3RALLY_VERSION_ARG

ENV ARCH=$ARCH_ARG
ENV LANG=$LANG_ARG
ENV TIMEZONE=$TIMEZONE_ARG
ENV Q3RALLY_VERSION=$Q3RALLY_VERSION_ARG

RUN apt-get update && \
  apt-get upgrade -y

RUN apt-get install -y locales && \
  sed -i -e "s/# $LANG.*/$LANG UTF-8/" /etc/locale.gen && \
  dpkg-reconfigure locales && \
  update-locale LANG=$LANG

RUN apt-get install -y tzdata && \ 
  ln -sf "/usr/share/zoneinfo/$TIMEZONE" /etc/localtime && \
  dpkg-reconfigure tzdata

RUN apt-get install -y \
  ca-certificates \
  lsb-release \
  --no-install-recommends

RUN mkdir /srv/q3rally

RUN useradd q3rally -r -d /srv/q3rally -s /bin/bash
RUN chown q3rally:q3rally /srv/q3rally

FROM base AS builder

RUN apt-get install -y \
  git \
  gcc \
  make \
  libc6-dev \
  unzip \
  wget2 \
  --no-install-recommends

USER q3rally
WORKDIR /srv/q3rally

RUN git clone --depth=1 -b v${Q3RALLY_VERSION} https://github.com/Q3Rally-Team/q3rally.git application

WORKDIR /srv/q3rally/application

RUN cd engine/ && \
  BUILD_STANDALONE=1 \
  BUILD_CLIENT=0 \
  BUILD_SERVER=1 \
  BUILD_GAME_SO=0 \
  BUILD_GAME_QVM=0 \
  BUILD_BASEGAME=0 \
  BUILD_MISSIONPACK=0 \
  BUILD_RENDERER_OPENGL2=0 \
  BUILD_AUTOUPDATER=0 \
  make

WORKDIR /srv/q3rally

RUN wget2 https://github.com/Q3Rally-Team/q3rally/releases/download/v${Q3RALLY_VERSION}/q3rally_v${Q3RALLY_VERSION}_linux64.zip

RUN unzip q3rally_v${Q3RALLY_VERSION}_linux64.zip -d application_content

FROM base AS final

RUN apt-get clean && \
  rm -rf /var/lib/apt/lists/*

USER q3rally
WORKDIR /srv/q3rally

RUN mkdir /srv/q3rally/baseq3r
  
COPY --from=builder \
  /srv/q3rally/application/engine/build/release-linux-${ARCH}/q3rally-server.${ARCH} \
  q3rally-server
COPY --from=builder \
  /srv/q3rally/application_content/Q3Rally/baseq3r/pak0.pk3 \
  /srv/q3rally/application_content/Q3Rally/baseq3r/default.cfg \
  baseq3r/
COPY ./config/ baseq3r/

USER root

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER q3rally

EXPOSE 27960/tcp 27960/udp
VOLUME /srv/q3rally/baseq3r

ENTRYPOINT /entrypoint.sh
