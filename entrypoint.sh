#!/bin/sh

set -e # exit inmediately on command execution failure
set -u # treat unset variables as an error when substituting
set -x # show commands when executing

EXECUTION_PATH='/srv/q3rally'

if [ -z "${DEDICATED}" ]
then
	DEDICATED=2
else
	if [ "${DEDICATED}" = "true" ]
	then
		DEDICATED=2
	else
		DEDICATED=1
	fi
fi

if [ -z "${DESCRIPTION}" ]
then
	DESCRIPTION='My default Q3Rally server'
fi

if [ -z "${GAME}" ]
then
	CONFIG='q3r_racing.cfg'
else
	CONFIG="q3r_${GAME}.cfg"
fi

exec "${EXECUTION_PATH}/q3rally-server" +set dedicated "$DEDICATED" +set sv_hostname "$DESCRIPTION" +exec "$CONFIG"
